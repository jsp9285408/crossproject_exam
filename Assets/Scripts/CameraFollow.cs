using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public float Smoothing = 5f;

    Vector3 MOffsetVal;

    // Start is called before the first frame update
    void Start()
    {
        MOffsetVal = Target.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetcamerapos = Target.position - MOffsetVal;
        transform.position = Vector3.Lerp(transform.position, targetcamerapos, Smoothing*Time.deltaTime);
    }
}
