using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarRoad : MonoBehaviour
{

    public Transform CloneTarget = null;
    public Transform GenerationPos = null;
    public int GenerationPersent = 50;
    public int randomval = 0;
    public float CloneDelaySec = 1.0f;

    protected float NextSecToClone = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float currSec = Time.time;
        if(NextSecToClone <= currSec)
        {
            randomval = Random.Range(0, 100);
            if(randomval <= GenerationPersent)
            {
                CloneCar();
            }
            NextSecToClone = Time.time + CloneDelaySec;
        }
    }

    void CloneCar()
    {
        Transform clonepos = GenerationPos;
        Vector3 offsetpos = clonepos.position;
        offsetpos.y = 1.0f;

        GameObject cloneobj = GameObject.Instantiate(
            CloneTarget.gameObject, offsetpos, GenerationPos.rotation, this.transform);
    }
}
