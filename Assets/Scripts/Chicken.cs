using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Chicken : MonoBehaviour
{
    protected Rigidbody ChickenBody = null;

    // Start is called before the first frame update
    void Start()
    {
        string[] templayer = new string[] { "Plant" };
        MTreeLayerMask = LayerMask.GetMask(templayer);
    }

    public enum EDirectionType
    {
        Up = 0,
        Down,
        Left,
        Right
    }

    [SerializeField]
    protected EDirectionType mDirectionType = EDirectionType.Up;
    protected int MTreeLayerMask = -1;
    protected bool ISCheckDirectionViewMove(EDirectionType Pmovetype)
    {
        Vector3 direction = Vector3.zero;

        switch (Pmovetype)
        {
            case EDirectionType.Up:
                {
                    direction = Vector3.forward;

                }
                break;
            case EDirectionType.Down:
                {
                    direction = Vector3.back;

                }
                break;
            case EDirectionType.Left:
                {
                    direction = Vector3.left;

                }
                break;
            case EDirectionType.Right:
                {
                    direction = Vector3.right;

                }
                break;
            default:
                Debug.LogErrorFormat("SetActorMove Error : {0}", Pmovetype);
                break;
        }

        RaycastHit hitobj;
        if(Physics.Raycast(this.transform.position, direction, out hitobj, 1f, MTreeLayerMask))
        {
            return false;
        }

        return true;
    }

    protected void SetActorMove(EDirectionType Pmovetype)
    {
        if(!ISCheckDirectionViewMove(Pmovetype))
        {
            return;
        }

        Vector3 offsetpos = Vector3.zero;

        switch (Pmovetype)
        {
            case EDirectionType.Up:
                {
                offsetpos = Vector3.forward;

                }
                break;
            case EDirectionType.Down:
                {
                offsetpos = Vector3.back;

                }
                break;
            case EDirectionType.Left:
                {
                offsetpos = Vector3.left;

                }
                break;
            case EDirectionType.Right:
                {
                offsetpos = Vector3.right;

                }
                break;
            default:
                Debug.LogErrorFormat("SetActorMove Error : {0}", Pmovetype);
                break;
        }

        this.transform.position += offsetpos;

        MWoodOffsetpos += offsetpos;
    }

    protected void InputUpdate()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            SetActorMove(EDirectionType.Up);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SetActorMove(EDirectionType.Down);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            SetActorMove(EDirectionType.Left);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            SetActorMove(EDirectionType.Right);
        }
    }

    Vector3 MWoodOffsetpos = Vector3.zero;
    protected void UpdateWood()
    {
        if(WoodObject == null)
        {
            return;
        }

        Vector3 actorpos = WoodObject.transform.position + MWoodOffsetpos;
        this.transform.position = actorpos;
    }

    // Update is called once per frame
    void Update()
    {
        InputUpdate();
        UpdateWood();

        Vector3 offsetpos = Vector3.zero;

        

        //this.transform.Translate();
    }


    [SerializeField]
    protected Wood WoodObject = null;
    protected Transform WoodCompareObj = null;
    protected void OnTriggerEnter(Collider other)
    {
        Debug.LogFormat("OnTriggerEnter : {0}, {1}", other.name, other.tag);

        if(other.tag.Contains("Wood"))
        {
            WoodObject = other.transform.parent.GetComponent<Wood>();

            if(WoodObject != null)
            {
                WoodCompareObj = WoodObject.transform;
                MWoodOffsetpos = other.transform.position - WoodObject.transform.position;
            }

            Debug.LogFormat("On Wood : {0}, {1}", other.name,MWoodOffsetpos);
            return;
        }

        if (other.tag.Contains("Crash"))
        {
            Debug.LogFormat("Crashed!!");
        }


    }

    protected void OnTriggerExit(Collider other)
    {
        Debug.LogFormat("OnTriggerExit : {0}, {1}", other.name, other.tag);

        if (other.tag.Contains("Wood") && WoodCompareObj == other.transform.parent)
        {
            WoodCompareObj = null;
            WoodObject = null;
            MWoodOffsetpos = Vector3.zero;
        }
    }

}
