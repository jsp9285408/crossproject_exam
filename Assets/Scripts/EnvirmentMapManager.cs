using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class EnvirmentMapManager : MonoBehaviour
{

    public GameObject[] EnvirmentObjectArray;
    public Transform ParentTransform = null;

    public int MinPosZ = -30;
    public int MaxPosZ = 30;
    // Start is called before the first frame update
    void Start()
    {

        for(int i=MinPosZ; i<MaxPosZ;++i)
        {
            CloneRoad(i);
        }


    }

    void CloneRoad(int PposZ)
    {
        int randomindex = Random.Range(0, EnvirmentObjectArray.Length);
        GameObject cloneobj = GameObject.Instantiate(EnvirmentObjectArray[randomindex]);
        cloneobj.SetActive(true);
        Vector3 offsetpos = Vector3.zero;
        offsetpos.z = (float)PposZ;
        cloneobj.transform.SetParent(ParentTransform);
        cloneobj.transform.position = offsetpos;

        int randomrot = Random.Range(0, 2);
        if(randomrot == 1 )
        {
            cloneobj.transform.rotation = Quaternion.Euler(0, 180f, 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
